import discord
import os
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv("BOT_TOKEN")
DEFAULT_ROLE_ID = int(os.getenv("DEFAULT_ROLE_ID"))

intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix="!", intents=intents)

@bot.event
async def on_ready():
    print(f'Bot is ready. Logged in as {bot.user}')
    for guild in bot.guilds:
        role = discord.Object(DEFAULT_ROLE_ID)
        for member in guild.members:
            if not member.bot and role not in member.roles:
                await member.add_roles(role)
                print(f'Assigned role to {member.name}')

@bot.event
async def on_member_join(member):
    role = discord.Object(DEFAULT_ROLE_ID)
    await member.add_roles(role)
    print(f'Assigned role to {member.name} on join')

bot.run(TOKEN)