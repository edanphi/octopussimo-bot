import subprocess

def redeploy_container():
    # Stop and remove the existing container if it's running
    subprocess.run(["docker", "stop", "octopussimo-bot"])
    subprocess.run(["docker", "rm", "octopussimo-bot"])

    # Build the Docker image with no cache
    subprocess.run(["docker", "build", "--no-cache=true", "-t", "octopussimo-bot:latest", "."])

    # Run the new container
    subprocess.run(["docker", "run", "--restart", "unless-stopped", "--network=host", "-d", "--name", "octopussimo-bot", "octopussimo-bot:latest"])

if __name__ == "__main__":
    redeploy_container()